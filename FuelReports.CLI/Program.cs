﻿using Commander.NET;
using Commander.NET.Exceptions;
using FuelReports.CLI.Commander.NET;
using System;
using System.Data.SqlClient;

namespace FuelReports.CLI
{
    class Program
    {    
        static void Main(string[] args)
        {
            try
            {
                CommanderParser<Options> parser = new CommanderParser<Options>();
                Options config = parser.Add(args).Parse();                
            }
            catch (CommandMissingException ex)
            {
                Console.WriteLine("Missing command: " + ex.Message);
            }
            catch (ParameterMissingException ex)
            {
                Console.WriteLine("Missing parameter: " + ex.ParameterName);
            }
            catch (SqlException sqle) 
            {
                Console.WriteLine(sqle.Message);
            }
            catch (ArgumentException argE)
            {
                Console.WriteLine($"Exception: {argE.Message}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e.Message}");

                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                }

            }
        }
    }
}
