﻿using Commander.NET.Interfaces;
using FuelReports.Data.Repositories;
using FuelReports.Services;
using FuelReports.SFTPDownloader;
using FuelReports.XMLDeserialization;
using FuelReports.XMLDeserialization.Models;
using System;
using System.Collections.Generic;

namespace FuelReports.CLI.Commander.NET
{
    class Process : ICommand
    {
        public void Execute(object parent)
        {
            Configuration configuration = new Configuration();
            configuration.mapperConfiguration.InitializeMapper();

            SftpService sftpService = new SftpService(configuration.sftpConfiguration);
            XmlService xmlService = new XmlService(configuration.sftpConfiguration.LocalDirectory);

            PetrolStationRepository petrolStationRepository = new PetrolStationRepository(configuration.connectionString);
            PetrolStationService petrolStationService = new PetrolStationService(petrolStationRepository);

            FuelPriceRecordRepository fuelPriceRecordRepository = new FuelPriceRecordRepository(configuration.connectionString);
            FuelPriceRecordService fuelPriceRecordService = new FuelPriceRecordService(fuelPriceRecordRepository);

            DailyRecordService dailyRecordService = new DailyRecordService(petrolStationService, fuelPriceRecordService);

            sftpService.DownloadAllFiles();

            Console.WriteLine();
            Console.WriteLine("All files have been downloaded");
            Console.WriteLine("Writting into database..");
            Console.WriteLine();

            List<DailyRecordModel> dailyRecords = xmlService.GetAllFilesData(sftpService.GetAllFilesNames());

            dailyRecordService.InsertMany(dailyRecords);

            Console.WriteLine("Data insertion completed successfully");
            Console.WriteLine($"Number of new deserialized files: {dailyRecords.Count}");
        }
    }
}
