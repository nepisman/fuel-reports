﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using System;
using System.Configuration;
using System.IO;

namespace FuelReports.CLI.Commander.NET
{
    class Config : ICommand
    {
        [Parameter("--data-dir", Required = Required.Yes)]
        public string dataDir;

        public void Execute(object parent)
        {
            if (!string.IsNullOrEmpty(dataDir))
            {
                if (!Directory.Exists(dataDir))
                {
                    Console.WriteLine("Directory not found.");
                }
                else
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    configuration.AppSettings.Settings["localDirectory"].Value = dataDir;
                    configuration.Save(ConfigurationSaveMode.Modified);

                    ConfigurationManager.RefreshSection("appSettings");

                    Console.WriteLine($"Files location: {ConfigurationManager.AppSettings.Get("localDirectory")}");
                }
            }
        }
    }
}
