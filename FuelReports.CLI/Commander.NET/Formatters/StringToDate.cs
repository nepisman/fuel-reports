﻿using Commander.NET.Interfaces;
using System;

namespace FuelReports.CLI.Commander.NET.Formatters
{
    class StringToDate : IParameterFormatter
    {
        public object Format(string name, string value)
        {
            string[] dateFormatter = value.Split('-');

            int year = Convert.ToInt32(dateFormatter[0]);
            int month = dateFormatter.Length > 1 ? Convert.ToInt32(dateFormatter[1]) : 1;
            int day = dateFormatter.Length > 2 ? Convert.ToInt32(dateFormatter[2]) : 1;

            return new DateTime(year, month, day);            
        }
    }
}
