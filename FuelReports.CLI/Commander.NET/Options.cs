﻿using Commander.NET.Attributes;

namespace FuelReports.CLI.Commander.NET
{
    class Options
    {
        [Command("config")]
        public Config Config;

        [Command("process")]
        public Process Process;

        [Command("report")]
        public Report Report;     
    }
}
