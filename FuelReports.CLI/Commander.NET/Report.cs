﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using FuelReports.Contracts.DTOs;
using FuelReports.Data.Repositories;
using FuelReports.Services;
using System;
using System.Collections.Generic;

namespace FuelReports.CLI.Commander.NET
{
    class Report : ICommand
    {
        [Parameter("--period", Required = Required.Yes)]
        public string period;

        [Parameter("--fuel-type", Required = Required.No)]
        public string fuelType;

        [Parameter("--petrol-station", Required = Required.No)]
        public string petrolStation;

        [Parameter("--city", Required = Required.No)]
        public string city;

        public void Execute(object parent)
        {
            Configuration configuration = new Configuration();
            string connectionString = configuration.connectionString;

            ReportRepository reportRepository = new ReportRepository(connectionString);
            ReportService reportService = new ReportService(reportRepository);

            string[] dateParts = period.Split('-');

            int year = Convert.ToInt32(dateParts[0]);
            int? month = dateParts.Length > 1 ? Convert.ToInt32(dateParts[1]) : null;
            int? day = dateParts.Length > 2 ? Convert.ToInt32(dateParts[2]) : null;

            FuelType? type = reportService.GetFuelType(fuelType);

            ReportDto reportDto = reportService.GetReport(petrolStation, city, year, month, day, type);

            PrintReport(reportDto, reportService);
        }

        private void PrintReport(ReportDto reportDto, ReportService reportService) 
        {
            Console.WriteLine();
            Console.WriteLine("--------------- R E P O R T ---------------");
            Console.WriteLine();
            Console.WriteLine($"Period: {period}");

            if (reportDto.ChainName != null)
                Console.WriteLine($"Petrol Station: {reportDto.ChainName}");

            if (reportDto.City != null)
                Console.WriteLine($"City: {reportDto.City}");

            Console.WriteLine();

            foreach (KeyValuePair<FuelType, decimal> kvp in reportDto.FuelTypePriceValues)
                Console.WriteLine($"Average price for {reportService.GetFuelTypeName(kvp.Key)} : {kvp.Value:c2}");
            
            Console.WriteLine();
            Console.WriteLine("-------------------------------------------");
        }
    }
}
