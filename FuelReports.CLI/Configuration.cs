﻿using FuelReports.Data;
using FuelReports.SFTPDownloader;
using System.Configuration;

namespace FuelReports.CLI
{
    public class Configuration
    {
        #region sftp configuration

        public SftpConfiguration sftpConfiguration = new SftpConfiguration
        {
            Credentials = new SftpCredentials
            {
                SftpHost = ConfigurationManager.AppSettings.Get("sftpHost"),
                Username = ConfigurationManager.AppSettings.Get("username"),
                Password = ConfigurationManager.AppSettings.Get("password"),
                Port = 22
            },
            RemoteDirectory = ConfigurationManager.AppSettings.Get("remoteDirectory"),
            LocalDirectory = ConfigurationManager.AppSettings.Get("localDirectory")
        };

        #endregion sftp configuration

        #region connection string

        public string connectionString = ConfigurationManager.ConnectionStrings["FuelReportsDBConnection"].ConnectionString;

        #endregion connection string

        #region automapper configuration

        public AutoMapperConfiguration mapperConfiguration = new AutoMapperConfiguration();

        #endregion automapper configuration
    }
}
