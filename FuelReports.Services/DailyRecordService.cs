﻿using FuelReports.Contracts.DTOs;
using FuelReports.Contracts.Interfaces.ServiceInterfaces;
using FuelReports.XMLDeserialization.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuelReports.Services
{
    public class DailyRecordService : IDailyRecordService
    {    
        private readonly PetrolStationService _petrolStationService;
        private readonly FuelPriceRecordService _fuelPriceRecordService;
        
        public DailyRecordService(PetrolStationService petrolStationService, FuelPriceRecordService fuelPriceRecordService) 
        {
            _petrolStationService = petrolStationService;
            _fuelPriceRecordService = fuelPriceRecordService;
        }
   
        public void InsertMany(List<DailyRecordModel> dailyRecords)
        {
            _petrolStationService.InsertMany(MapPetrolStations(dailyRecords));

            _fuelPriceRecordService.InsertMany(MapFuelPriceRecords(dailyRecords));
        }

        private List<PetrolStationDto> MapPetrolStations(List<DailyRecordModel> dailyRecords) 
        {
            List<PetrolStationDto> petrolStationDtos = new List<PetrolStationDto>();

            foreach (DailyRecordModel dailyRecord in dailyRecords) 
            {
                foreach (var petrolStation in dailyRecord.PetrolStations)
                {
                    PetrolStationDto petrolStationDto = new PetrolStationDto
                    {
                        Id = Guid.NewGuid(),
                        ChainName = petrolStation.Name,
                        City = petrolStation.City,
                        StreetAddress = petrolStation.Address
                    };

                    petrolStationDtos.Add(petrolStationDto);
                }            
            }

            return petrolStationDtos.GroupBy(ps => (ps.ChainName, ps.City, ps.StreetAddress))
                                    .Select(group => group.First()).ToList();
        }

        private List<FuelPriceRecordDto> MapFuelPriceRecords(List<DailyRecordModel> dailyRecords) 
        {
            List<FuelPriceRecordDto> fuelPriceRecordDtos = new List<FuelPriceRecordDto>();

            foreach (DailyRecordModel dailyRecord in dailyRecords) 
            {
                foreach (var petrolStation in dailyRecord.PetrolStations) 
                {
                    Guid petrolStationId = _petrolStationService.GetPetrolStationId(petrolStation.Name,
                                                                                    petrolStation.City,
                                                                                    petrolStation.Address);

                    foreach (var fuel in petrolStation.Fuels) 
                    {
                        FuelPriceRecordDto fuelPriceRecordDto = new FuelPriceRecordDto
                        {
                            Id = Guid.NewGuid(),
                            Date = dailyRecord.Date,
                            FuelType = (Contracts.DTOs.FuelType)fuel.FuelType,
                            Price = Math.Round(decimal.Parse(fuel.Price.TrimStart('$')), 2),
                            PetrolStationId = petrolStationId
                        };

                        fuelPriceRecordDtos.Add(fuelPriceRecordDto);
                    }                                
                }
            }

            return fuelPriceRecordDtos;
        }
    }
}
