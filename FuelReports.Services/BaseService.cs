﻿using FuelReports.Contracts.DTOs;
using FuelReports.Contracts.Interfaces.RepositoryInterfaces;
using System;
using System.Collections.Generic;

namespace FuelReports.Services
{
    public abstract class BaseService<TDto, TRepository>
        where TDto : BaseDto
        where TRepository : IBaseRepository<TDto>
    {
        protected readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public void Insert(TDto dto) 
        {
            if (dto == null) 
            {
                throw new ArgumentNullException(nameof(dto));
            }

            _repository.Insert(dto);
        }

        public void InsertMany(List<TDto> dtos) 
        {
            if (dtos == null) 
            {
                throw new ArgumentNullException(nameof(dtos));
            }

            _repository.InsertMany(dtos);          
        }
    }
}
