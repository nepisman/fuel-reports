﻿using FuelReports.Contracts.DTOs;
using FuelReports.Data.Repositories;

namespace FuelReports.Services
{
    public class ReportService
    {
        private readonly ReportRepository _reportRepository;

        public ReportService(ReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }

        public ReportDto GetReport(string petrolStaton, string city, int year, int? month, int? day, FuelType? fuelType = null) 
        {                       
            return _reportRepository.GetReport(petrolStaton, city, year, month, day, fuelType);
        }
     
        public FuelType? GetFuelType(string fuelType) 
        {
            FuelType? type;

            switch (fuelType) 
            {
                case "Petrol":
                    type = FuelType.Petrol;
                    break;

                case "Premium Petrol":
                    type = FuelType.PremiumPetrol;
                    break;

                case "Diesel":
                    type = FuelType.Diesel;
                    break;

                case "Premium Diesel":
                    type = FuelType.PremiumDiesel;
                    break;

                case "LPG":
                    type = FuelType.LPG;
                    break;

                case "CNG":
                    type = FuelType.CNG;
                    break;

                default:
                    return null;
            }

            return (FuelType)type;
        }

        public string GetFuelTypeName(FuelType fuelType) 
        {
            string fuelTypeName = null;

            switch (fuelType) 
            {
                case FuelType.Petrol:
                    fuelTypeName = "Petrol";
                    break;

                case FuelType.PremiumPetrol:
                    fuelTypeName = "Premium Petrol";
                    break;

                case FuelType.Diesel:
                    fuelTypeName = "Diesel";
                    break;

                case FuelType.PremiumDiesel:
                    fuelTypeName = "Premium Diesel";
                    break;

                case FuelType.LPG:
                    fuelTypeName = "LPG";
                    break;

                case FuelType.CNG:
                    fuelTypeName = "CNG";
                    break;
            }

            return fuelTypeName;
        }      
    }
}
