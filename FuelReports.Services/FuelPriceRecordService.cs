﻿using FuelReports.Contracts.DTOs;
using FuelReports.Data.Repositories;

namespace FuelReports.Services
{
    public class FuelPriceRecordService : BaseService<FuelPriceRecordDto, FuelPriceRecordRepository>
    {                
        public FuelPriceRecordService(FuelPriceRecordRepository repository) : base(repository)
        {
            
        }      
    }
}
