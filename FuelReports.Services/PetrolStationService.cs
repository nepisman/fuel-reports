﻿using FuelReports.Contracts.DTOs;
using FuelReports.Data.Repositories;
using System;

namespace FuelReports.Services
{
    public class PetrolStationService : BaseService<PetrolStationDto, PetrolStationRepository>
    {
        public PetrolStationService(PetrolStationRepository repository) : base(repository) 
        {         
             
        }     

        public Guid GetPetrolStationId(string chainName, string city, string streetAddress) 
        {
            return _repository.GetPetrolStationId(chainName, city, streetAddress);
        }

        public bool CheckIfExist(PetrolStationDto petrolStationDto) 
        {
            return _repository.CheckIfExists(petrolStationDto);
        }
    }
}
