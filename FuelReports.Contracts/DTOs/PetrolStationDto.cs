﻿namespace FuelReports.Contracts.DTOs
{
    public class PetrolStationDto : BaseDto
    {
        public string ChainName { get; set; }

        public string City { get; set; }

        public string  StreetAddress { get; set; }
    }
}
