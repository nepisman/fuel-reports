﻿using System;

namespace FuelReports.Contracts.DTOs
{
    public class FuelPriceRecordDto : BaseDto
    {
        public DateTime Date { get; set; }

        public decimal Price { get; set; }

        public FuelType FuelType { get; set; }

        public Guid PetrolStationId { get; set; }
    }
}
