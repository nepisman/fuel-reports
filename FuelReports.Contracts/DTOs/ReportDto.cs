﻿using System.Collections.Generic;

namespace FuelReports.Contracts.DTOs
{
    public class ReportDto
    {
        public int? Year { get; set; }

        public int? Month { get; set; }

        public int? Day { get; set; }

        public SortedDictionary<FuelType, decimal> FuelTypePriceValues  { get; set; }

        public string ChainName { get; set; }

        public string City { get; set; }
    }
}
