﻿namespace FuelReports.Contracts.DTOs
{
    public enum FuelType
    {
        Petrol,
        PremiumPetrol,
        Diesel,
        PremiumDiesel,
        LPG,
        CNG
    }
}
