﻿using System;

namespace FuelReports.Contracts.DTOs
{
    public abstract class BaseDto
    {
        public Guid Id { get; set; }
    }
}
