﻿using FuelReports.XMLDeserialization.Models;
using System.Collections.Generic;

namespace FuelReports.Contracts.Interfaces.ServiceInterfaces
{
    public interface IDailyRecordService
    {
        void InsertMany(List<DailyRecordModel> dailyRecords);
    }
}
