﻿using FuelReports.Contracts.DTOs;
using System;

namespace FuelReports.Contracts.Interfaces.RepositoryInterfaces
{
    public interface IPetrolStationRepository : IBaseRepository<PetrolStationDto>
    {
        Guid GetPetrolStationId(string chainName, string city, string streetAddress);
    }
}
