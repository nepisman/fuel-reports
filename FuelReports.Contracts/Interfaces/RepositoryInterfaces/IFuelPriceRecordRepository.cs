﻿using FuelReports.Contracts.DTOs;

namespace FuelReports.Contracts.Interfaces.RepositoryInterfaces
{
    public interface IFuelPriceRecordRepository : IBaseRepository<FuelPriceRecordDto>
    {
      
    }
}
