﻿using FuelReports.Contracts.DTOs;
using System.Collections.Generic;

namespace FuelReports.Contracts.Interfaces.RepositoryInterfaces
{
    public interface IBaseRepository<TDto>
        where TDto : BaseDto
    {
        void Insert(TDto dto);

        void InsertMany(List<TDto> dtos);
    }
}
