﻿using FuelReports.Contracts.DTOs;

namespace FuelReports.Contracts.Interfaces.RepositoryInterfaces
{
    public interface IReportRepository
    {
        ReportDto GetReport(string petrolStaton, string city, int year, int? month, int? day, FuelType? fuelType = null);
    }
}
