﻿using FuelReports.XMLDeserialization.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace FuelReports.XMLDeserialization
{
    public class XmlService
    {
        private readonly string _xmlFilesDirectory;

        public XmlService(string xmlFilesDirectory) 
        {
            _xmlFilesDirectory = xmlFilesDirectory;
        }

        public List<DailyRecordModel> GetAllFilesData(List<string> fileNames)
        {
            List<DailyRecordModel> dailyRecords = new List<DailyRecordModel>();

            foreach (string fileName in fileNames) 
            {
                dailyRecords.Add(GetFileData(fileName));                
            }

            return dailyRecords;
        }

        public DailyRecordModel GetFileData(string fileName) 
        {
            string filePath = _xmlFilesDirectory + $"/{fileName}";

            return DeserializeXmlFile<DailyRecordModel>(filePath);            
        }

        public T DeserializeXmlFile<T>(string filePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            T deserializationObject = default; 

            if (filePath == null)
            {
                throw new ArgumentNullException("No xml file is given");
            }

            using (Stream reader = new FileStream(filePath, FileMode.Open))
            {
                deserializationObject = (T)xmlSerializer.Deserialize(reader);
            }

            return deserializationObject;
        }
    }
}
