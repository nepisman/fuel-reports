﻿using System.Xml.Serialization;

namespace FuelReports.XMLDeserialization.Models
{
    [XmlRoot("fuel")]
    public class FuelModel
    {
        [XmlAttribute("type")]
        public FuelType FuelType { get; set; }

        [XmlElement("price")]
        public string Price { get; set; }        
    }
}
