﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace FuelReports.XMLDeserialization.Models
{
    [XmlRoot("petrolStation")]
    public class PetrolStationModel 
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlAttribute("city")]
        public string City { get; set; }

        [XmlArray(ElementName ="fuels")]
        [XmlArrayItem(Type = typeof(FuelModel), ElementName = "fuel")]
        public List<FuelModel> Fuels { get; set; }
    }
}
