﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FuelReports.XMLDeserialization.Models
{
    [XmlRoot("petrolStations")]
    public class DailyRecordModel 
    {
        [XmlAttribute("date")]
        public DateTime Date { get; set; }

        [XmlElement(ElementName = "petrolStation")]
        public List<PetrolStationModel> PetrolStations { get; set; }
    }
}
