﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FuelReports.XMLDeserialization.Models
{
    public enum FuelType
    {
        Petrol,
        PremiumPetrol,
        Diesel,
        PremiumDiesel,
        LPG,
        CNG
    }
}
