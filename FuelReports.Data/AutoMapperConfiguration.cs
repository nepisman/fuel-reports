﻿using AutoMapper;
using FuelReports.Contracts.DTOs;
using FuelReports.Data.Entities;
using System;

namespace FuelReports.Data
{
    public class AutoMapperConfiguration
    {
        [Obsolete]
        public void InitializeMapper()
        {
            Mapper.Initialize(m =>
            {
                m.CreateMap<PetrolStationEntity, PetrolStationDto>().ReverseMap();
                m.CreateMap<FuelPriceRecordDto, FuelPriceRecordEntity>().ReverseMap();
            });
        }
    }
}
