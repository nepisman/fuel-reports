﻿using FuelReports.Contracts.DTOs;
using FuelReports.Contracts.Interfaces.RepositoryInterfaces;
using FuelReports.Data.Entities;

namespace FuelReports.Data.Repositories
{
    public class FuelPriceRecordRepository :
        BaseRepository<FuelPriceRecordDto, FuelPriceRecordEntity>,
        IFuelPriceRecordRepository
    {
        public FuelPriceRecordRepository(string connectionString) : base(connectionString)
        {
           
        }
    }
}
