﻿using AutoMapper;
using FuelReports.Contracts.DTOs;
using FuelReports.Contracts.Interfaces.RepositoryInterfaces;
using FuelReports.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace FuelReports.Data.Repositories
{
    public abstract class BaseRepository<TDto, TEntity> : IBaseRepository<TDto>
        where TDto : BaseDto, new()
        where TEntity : BaseEntity, new()
    {
        protected readonly string _connectionString;

        protected BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public virtual void Insert(TDto dto)
        {            
            using SqlConnection connection = new SqlConnection(_connectionString);
            using SqlCommand command = BuildSqlCommand(dto, connection);

            connection.Open();
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
        }

        public virtual void InsertMany(List<TDto> dtos)
        {
            using SqlConnection connection = new SqlConnection(_connectionString);

            connection.Open();

            foreach (TDto dto in dtos)
            {
                using SqlCommand command = BuildSqlCommand(dto, connection);

                command.ExecuteNonQuery();
                command.Dispose();
            }

            connection.Close();
        }

        protected SqlCommand BuildSqlCommand(TDto dto, SqlConnection connection) 
        {
            var entity = Mapper.Map<TEntity>(dto);
            Type entityType = typeof(TEntity);

            string tableName = entityType.Name.TrimEnd().Replace("Entity", "");
            IEnumerable<string> columns = entityType.GetProperties().Select(prop => prop.Name);
            IEnumerable<string> values = columns.Select(col => $"@{col}");

            string queryColumnString = string.Join(',', columns);
            string queryValuesString = string.Join(',', values);

            string query = $"INSERT INTO {tableName} ({queryColumnString}) VALUES ({queryValuesString})";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddRange(GetParameters(entity));

            return command;
        }

        protected SqlParameter[] GetParameters(TEntity entity) 
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity).GetProperties();
            List<SqlParameter> parameters = new List<SqlParameter>();

            foreach (var prop in properties) 
            {
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = $"@{prop.Name}";
                parameter.Value = prop.GetValue(entity) == null ? DBNull.Value : prop.GetValue(entity);

                parameters.Add(parameter);
            }

            return parameters.ToArray();
        }
    }
}
