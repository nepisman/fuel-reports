﻿using FuelReports.Contracts.DTOs;
using FuelReports.Contracts.Interfaces.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace FuelReports.Data.Repositories
{
    public class ReportRepository : IReportRepository
    {
        private readonly string tableName = "FuelPriceRecord";
        private readonly string _connectionString;

        public ReportRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ReportDto GetReport(string petrolStaton, string city, int year, int? month, int? day, FuelType? fuelType = null)
        {
            string query = BuildSqlQuery(petrolStaton, city, year, month, day, fuelType);

            using SqlConnection connection = new SqlConnection(_connectionString);
            using SqlCommand command = connection.CreateCommand();
            command.CommandText = query;         
            command.Parameters.AddRange(GetParameters(petrolStaton, city, year, month, day, fuelType));
                    
            ReportDto reportDto = new ReportDto 
            {
                ChainName = petrolStaton,
                City = city,
                Year = year,
                Month = month,
                FuelTypePriceValues = new SortedDictionary<FuelType, decimal>()  
            };

            connection.Open();
            using (SqlDataReader reader = command.ExecuteReader()) 
            {
                while (reader.Read())
                    reportDto.FuelTypePriceValues.Add((FuelType)Convert.ToInt32(reader[0]), (decimal)reader[1]);            
            }
            command.Dispose();
            connection.Close();

            return reportDto;
        }

        protected string BuildSqlQuery(string petrolStation, string city, int year, int? month, int? day, FuelType? fuelType = null) 
        {
            StringBuilder query = new StringBuilder();

            query.Append($"SELECT FuelType, AVG(Price) FROM {tableName}");

            if (!string.IsNullOrWhiteSpace(petrolStation) || !string.IsNullOrWhiteSpace(city))
                query.Append($" JOIN PetrolStation ON {tableName}.PetrolStationId=PetrolStation.Id");
            
            query.Append(BuildDateQueryPart(year, month, day));
            query.Append(BuildPetrolStationCityQueryPart(petrolStation, city));
            query.Append(BuildFuelTypeQueryPart(fuelType));

            return query.ToString();
        }

        protected string BuildDateQueryPart(int year, int? month, int? day) 
        {
            StringBuilder dateQuery = new StringBuilder();
           
            dateQuery.Append($" WHERE YEAR(Date)=@Year");

            if(month != null)
                dateQuery.Append($" AND MONTH(Date)=@Month");

            if (day != null)
                dateQuery.Append($" AND DAY(Date)=@Day");

            return dateQuery.ToString();
        }

        protected string BuildPetrolStationCityQueryPart(string petrolStation, string city) 
        {
            StringBuilder chainNameCityQuery = new StringBuilder();               

            if (petrolStation != null)
                chainNameCityQuery.Append($" AND ChainName=@ChainName");

            if (city != null)
                chainNameCityQuery.Append($" AND City=@City");
          
            return chainNameCityQuery.ToString();
        }

        protected string BuildFuelTypeQueryPart(FuelType? fuelType) 
        {
            StringBuilder fuelTypeQuery = new StringBuilder();

            if (fuelType == null)
                fuelTypeQuery.Append(" GROUP BY FuelType ORDER BY FuelType ASC");
           
            else
                fuelTypeQuery.Append($" GROUP BY FuelType HAVING FuelType=@FuelType");          

            return fuelTypeQuery.ToString();
        }

        protected SqlParameter[] GetParameters(string petrolStaton, string city, int year, int? month, int? day, FuelType? fuelType = null) 
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            AddNewSqlParameter(parameters, "@Year", year);

            if (petrolStaton != null)
                AddNewSqlParameter(parameters, "@ChainName", petrolStaton);

            if (city != null)
                AddNewSqlParameter(parameters, "@City", city);

            if (month != null)
                AddNewSqlParameter(parameters, "@Month", month);

            if (day != null)
                AddNewSqlParameter(parameters, "@Day", day);

            if (fuelType != null)
                AddNewSqlParameter(parameters, "@FuelType", fuelType);

            return parameters.ToArray();
        }

        protected void AddNewSqlParameter<T>(List<SqlParameter> parameters, string name, T value) 
        {
            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = name;
            parameter.Value = value;

            parameters.Add(parameter);
        }
    }
}
