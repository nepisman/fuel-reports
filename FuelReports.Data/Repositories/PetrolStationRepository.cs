﻿using FuelReports.Contracts.DTOs;
using FuelReports.Contracts.Interfaces.RepositoryInterfaces;
using FuelReports.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace FuelReports.Data.Repositories
{
    public class PetrolStationRepository : 
        BaseRepository<PetrolStationDto, PetrolStationEntity>,
        IPetrolStationRepository
    {
        private readonly string tableName = "PetrolStation";

        public PetrolStationRepository(string connectionString) : base(connectionString)
        {           

        }

        public override void Insert(PetrolStationDto petrolStationDto) 
        {
            if (!CheckIfExists(petrolStationDto))
                base.Insert(petrolStationDto);                
        }

        public override void InsertMany(List<PetrolStationDto> petrolStationDtos)
        {
            foreach (PetrolStationDto petrolStationDto in petrolStationDtos) 
            {
                if (CheckIfExists(petrolStationDto))
                    petrolStationDtos.Remove(petrolStationDto);
            }

            base.InsertMany(petrolStationDtos);
        } 

        public Guid GetPetrolStationId(string chainName, string city, string streetAddress)
        {
            Guid petrolStationId = default;

            string query = $"SELECT Id from {tableName} WHERE ChainName=@chainName AND City=@city AND StreetAddress=@streetAddress";
           
            using (SqlConnection connection = new SqlConnection(_connectionString)) 
            {
                using (SqlCommand command = new SqlCommand(query, connection)) 
                {
                    command.Parameters.AddWithValue("@chainName", chainName);
                    command.Parameters.AddWithValue("@city", city);
                    command.Parameters.AddWithValue("@streetAddress", streetAddress);

                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            petrolStationId = (Guid)reader[0];
                    }                    
                }
            }

            return petrolStationId;
        }

        public bool CheckIfExists(PetrolStationDto petrolStationDto) 
        {
            Guid checkGuid = GetPetrolStationId(petrolStationDto.ChainName,
                                                petrolStationDto.City,
                                                petrolStationDto.StreetAddress);

            return checkGuid != Guid.Empty;
        }
    }
}
