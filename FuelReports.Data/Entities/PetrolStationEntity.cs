﻿namespace FuelReports.Data.Entities
{
    public class PetrolStationEntity : BaseEntity
    {
        public string ChainName { get; set; }

        public string City { get; set; }

        public string StreetAddress { get; set; }
    }
}
