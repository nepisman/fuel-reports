﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FuelReports.Data.Entities
{
    public enum FuelType
    {
        Petrol,
        PremiumPetrol,
        Diesel,
        PremiumDiesel,
        LPG,
        CNG
    }
}
