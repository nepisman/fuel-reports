﻿using System;

namespace FuelReports.Data.Entities
{
    public class FuelPriceRecordEntity : BaseEntity
    {
        public DateTime Date { get; set; }

        public decimal Price { get; set; }

        public FuelType FuelType { get; set; }

        public Guid PetrolStationId { get; set; }
    }
}
