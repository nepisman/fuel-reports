﻿using System;

namespace FuelReports.Data.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
