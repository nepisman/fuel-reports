﻿namespace FuelReports.SFTPDownloader
{
    public class SftpCredentials
    {
        public string SftpHost { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int Port { get; set; }     
    }
}
