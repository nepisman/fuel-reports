﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Collections.Generic;
using System.IO;

namespace FuelReports.SFTPDownloader
{
    public class SftpService
    {
        private readonly SftpConfiguration _sftpConfiguration;

        public SftpService(SftpConfiguration sftpConfiguration)            
        {
            _sftpConfiguration = sftpConfiguration;
        }
        
        public void DownloadFile(string fileName)
        {
            string remoteFile = Path.Combine(_sftpConfiguration.RemoteDirectory, fileName);

            using (SftpClient sftpClient = CreateSftpClient()) 
            {
                sftpClient.Connect();

                using (Stream fileStream = File.OpenWrite(Path.Combine(_sftpConfiguration.LocalDirectory, fileName)))
                {
                    sftpClient.DownloadFile(remoteFile, fileStream);
                }

                sftpClient.Disconnect();
            }
        }

        public void DownloadAllFiles() 
        {
            var files = GetAllFiles();

            using (SftpClient sftpClient = CreateSftpClient())
            {
                sftpClient.Connect();

                foreach (var file in files)
                {
                    if (!file.IsDirectory && !file.IsSymbolicLink)
                    {
                        using (Stream fileStream = File.OpenWrite(Path.Combine(_sftpConfiguration.LocalDirectory, file.Name)))
                        {
                           sftpClient.DownloadFile(Path.Combine(_sftpConfiguration.RemoteDirectory, file.Name), fileStream);                        
                        }
                    }
                }

                sftpClient.Disconnect();
            }
        }

        public IEnumerable<SftpFile> GetAllFiles()
        {
            IEnumerable<SftpFile> files = new List<SftpFile>();

            using (SftpClient sftpClient = CreateSftpClient())
            {
                sftpClient.Connect();

                files = sftpClient.ListDirectory(_sftpConfiguration.RemoteDirectory);

                sftpClient.Disconnect();
            }

            return files;
        }

        public List<string> GetAllFilesNames() 
        {
            List<string> fileNames = new List<string>();

            foreach (var file in GetAllFiles()) 
            {
                if (!file.IsDirectory && !file.IsSymbolicLink) 
                {
                    fileNames.Add(file.Name);                
                }
            }

            return fileNames;
        }

        private SftpClient CreateSftpClient()
            => new SftpClient(_sftpConfiguration.Credentials.SftpHost, 
                              _sftpConfiguration.Credentials.Username,
                              _sftpConfiguration.Credentials.Password);    
    }
}
