﻿namespace FuelReports.SFTPDownloader
{
    public class SftpConfiguration
    {
        public SftpCredentials Credentials { get; set; }
        public string RemoteDirectory { get; set; }
        public string LocalDirectory { get; set; }
    }
}
